# Script to run different methods on Brain Cortex Data:
source('utilfunctions.R')
source('trends.R')
source('linearTrends.R')
source('KolmogorovSmirnov.R')
source('DREMI.R')
library(monocle)
library(VGAM)


## Read in data: ## 
load('BrainData/processed_genes.R') # load 'genes'
load('BrainData/processedFPKM.R') # load 'fpkm_values'
load('BrainData/cell_phenotypes.R') # load 'cell_phenotypes'
dim(fpkm_vals); head(fpkm_vals[,1:5])
head(genes)
head(cell_phenotypes)

## Log-transform
logexpr = log10(fpkm_vals+1)

## Restrict dataset to Cortex cells:
cortex_logexpr = logexpr[,cell_phenotypes$type == 'sscortex']
cortex_cell_phenotypes = cell_phenotypes[cell_phenotypes$type == 'sscortex',]
# Structure cell-time information:
cortex_cell_times = list()
cortex_ages = sort(unique(cortex_cell_phenotypes$age))
for (l in 1:length(cortex_ages)) {
    cortex_cell_times[[l]] = list()
    sub_pheno = cortex_cell_phenotypes[cortex_cell_phenotypes$age == cortex_ages[l],]
    tbl = table(sub_pheno$batch)
    for (i in 1:length(tbl)) {
        cortex_cell_times[[l]][[i]] = which(cortex_cell_phenotypes$batch == names(tbl)[i])
    }
}
rownames(cortex_logexpr) = 1:nrow(cortex_logexpr)

## Run TRENDS:
trends_results = analyzeSCRS(cortex_logexpr, cortex_cell_times, parallel_cores = 8,
                padjust = 'minP', ps = (1:99)/100,
                num_permutation= 1000, bootstrap = T, use_kernel = T)

save(trends_results, file="BrainData/trends_results.R")

## Run linear TRENDS on this data ##
lineartrend_results = linearTrendSCRS(cortex_logexpr, cortex_cell_times,
                                      parallel_cores=8, padjust = "minP", ps = (1:99)/100,
                                      num_permutation=1000, ls = cortex_ages,
                                      bootstrap = T, use_kernel = T)

save(lineartrend_results, file="BrainData/lineartrend_results.R")


## Run DREMI on this data ## 
time_vector = rep(0, ncol(cortex_logexpr));
for (l in 1:length(cortex_cell_times)) {
     for (i in 1:length(cortex_cell_times[[l]])) {
         time_vector[cortex_cell_times[[l]][[i]]] = l
     }  
}

dremi_res = DREMIanalysis(cortex_logexpr, time_vector, ngrid = 512, num_permutation = 1000,
                          padjust = 'minP', parallel_cores = 8, use_kernel = T)

save(dremi_results, file="BrainData/dremi_results.R")


## Run KS test on this data ##
ks_results = ksDEanalysis(cortex_logexpr, time_vector, num_permutation = 1000, 
                          padjust = "minP", parallel_cores = 8) 

save(ks_results, file="BrainData/ks_results.R")


## Run Monocle Tobit model:
cortex_expr = 10^(cortex_logexpr) - 1 # Must create cortex_expr with non-logged FPKM values because Monocle takes log.
time_vector = rep(0, ncol(cortex_expr));
for (l in 1:length(cortex_cell_times)) {
     for (i in 1:length(cortex_cell_times[[l]])) {
         time_vector[cortex_cell_times[[l]][[i]]] = cortex_ages[l]
     }
}
time_df = data.frame(time = time_vector)
rownames(time_df) = colnames(cortex_expr)

pd = new("AnnotatedDataFrame", data = time_df)
HSMM <- newCellDataSet(cortex_expr, phenoData = pd)

# Linear Tobit:
lineartobit_results <- differentialGeneTest(HSMM, fullModelFormulaStr="~time", reducedModelFormulaStr = "~1", cores = 1)
ordering = order(lineartobit_results$pval)
lineartobit_results = lineartobit_results[ordering,]
save(lineartobit_results, "BrainData/lineartobit_results.R")

# B-Spline Tobit:
bsmonocle_results <- differentialGeneTest(HSMM, fullModelFormulaStr="~sm.ns(time, df=3)", reducedModelFormulaStr = "~1", cores=1)
ordering = order(bsmonocle_results$pval)
bsmonocle_results = bsmonocle_results[ordering,]
save(bsmonocle_results, "BrainData/bsmonocle_results.R")
