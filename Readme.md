Code for the paper: 

J. Mueller, T. Jaakkola, D. Gifford. "Modeling Persistent Trends in Distributions". Journal of the American Statistical Association (2017)

URL: http://dx.doi.org/10.1080/01621459.2017.1341412


The file: trends.R contains our core functions for fitting the TRENDS model to a single response variable as well as a parallelized implementation for fitting the TRENDS model to thousands of genes in a scRNA-seq dataset, along with permutation tests to measure statistical significance.

The file: examplePlots.R demonstrates simple example usage of our TRENDS functions.

The raw files for the scRNA-seq expression data analyzed in our paper are obtained from the Gene Expression Omnibus (GEO):

Myoblast Data: 
URL = http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE52529
Filenames = GSE52529_fpkm_matrix.txt (Expression Profiles)
GSE52529-GPL16791_series_matrix.txt (Series Matrix File)
GSE52529-GPL11154_series_matrix.txt (Series Matrix File)

Brain Cortex Data: 
URL = http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE60361
Filenames = GSE60361_C1-3005-Expression.txt (Expression Profiles)
GSE60361_series_matrix.txt (Series Matrix File)

Our analysis of the Myoblast data was conducted via the scripts:
MyoblastDataPreprocess.R, MyoblastDataAnalysis.R, MyoblastDataEvaluatingResults.R

Our analysis of the Brain Cortex data was conducted via the scripts:
BrainDataPreprocess.R, BrainDataAnalysis.R, BrainDataEvaluatingResults.R


Full list of required R packages:
beeswarm
compiler
clinfun
Cairo
doParallel
foreach
inline
Iso
pracma
sROC
VGAM
vioplot

We also require the following Bioconductor packages:
biomaRt
monocle
org.Mm.eg.db
org.Hs.eg.db
GO.db

All of these dependencies can be installed using: 

install.packages(c("beeswarm","clinfun","Cairo","doParallel","foreach","inline","Iso","pracma","sROC","VGAM","vioplot"))
source("https://bioconductor.org/biocLite.R")
biocLite(c("monocle", "biomaRt","org.Mm.eg.db","org.Hs.eg.db","GO.db"))
