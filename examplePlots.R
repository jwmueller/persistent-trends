# Plots examples of non-trending distributions and the corresponding TRENDS-fits:
source('utilfunctions.R')
source('trends.R')

plotfile = 'Mean+Var_decreasing.pdf'
n = 1e5
L = 3
PP = 99
ps = (1:PP)/(PP+1)

x = initializeX(L)
x[[1]][[1]] = rnorm(n,mean = 3.5, sd = 2)
x[[2]][[1]] = rnorm(n,mean = 3, sd = 1.5)
x[[3]][[1]] = rnorm(n,mean = 1, sd = 0.4)
quants = getQuantiles(x, ps)
res = bilateralTF(quants, ps, x)
rsqr = as.numeric(format(round(res$R.sqr,3),nsmall=3))
plotQFs(QFmatrix(quants), res$fq, ps,
        outputfile=plotfile, drawpoints = F,
        delta.quant = "monotone", overalltitle = bquote(bold("(B)") ~~~ italic(R^2) == .(rsqr)),
        width = 12, height = 6.5)

plotfile = 'All_Over.pdf'
x = initializeX(L)
x[[1]][[1]] = rnorm(n,mean = 2.5, sd = 2)
x[[2]][[1]] = rnorm(n,mean = 0.3, sd = 1.5)
x[[3]][[1]] = rnorm(n,mean = 5, sd = 2)
quants = getQuantiles(x, ps)
res = bilateralTF(quants, ps, x)
rsqr = as.numeric(format(round(res$R.sqr,3),nsmall=3))
plotQFs(QFmatrix(quants), res$fq, ps, outputfile=plotfile, drawpoints = F,
        delta.quant = "monotone", overalltitle = bquote(bold("(A)") ~~~ italic(R^2) == .(rsqr)),
        width = 12, height = 6.5)

plotfile = 'Mixture5components.pdf'
x = initializeX(L)
x[[1]][[1]] = c(rnorm(n*0.2, mean = -6),rnorm(n*0.2, mean = -3),rnorm(n*0.2, mean = 0),
          rnorm(n*0.2, mean = 3),rnorm(n*0.2, mean = 6))
x[[2]][[1]] = c(rnorm(n*0.15, mean = -6),rnorm(n*0.3, mean = -3),rnorm(n*0.1, mean = 0),
           rnorm(n*0.3, mean = 3),rnorm(n*0.15, mean = 6))
x[[3]][[1]] = c(rnorm(n*0.05, mean = -6),rnorm(n*0.425, mean = -3),rnorm(n*0.05, mean = 0),
           rnorm(n*0.425, mean = 3),rnorm(n*0.05, mean = 6))
quants = getQuantiles(x, ps)
res = bilateralTF(quants, ps, x)
rsqr = as.numeric(format(round(res$R.sqr,3),nsmall=3))
plotQFs(QFmatrix(quants), res$fq, ps, outputfile=plotfile, drawpoints = F,
        delta.quant = "monotone", overalltitle = bquote(bold("(C)") ~~~ italic(R^2) == .(rsqr)),
        vioplot_h = 0.3, width = 12, height = 6.5)
