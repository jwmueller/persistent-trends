# Scripts to fit linear TRENDS model using Dykstra's method:
source('utilfunctions.R')

fitLinearTrends <- function(quants, ps = NULL, x = NULL,
                      weights = NULL, ls = NULL,
                      uniformly_spaced = NULL,
                      max.iter = 1e2, 
                      seed = 12345,
                      silent = FALSE) {
    L = length(quants)
    if (is.null(ls)) { ls = 1:L }
    if (is.null(ps)) {
        if (is.null(x)) {
            stop("ps and x cannot both be NULL")
        }
        ps = getDefaultProbs(x)
        uniformly_spaced = TRUE
    }
    PP = length(ps)
    extended.ps = c(2*ps[1] - ps[2], ps, 2*ps[PP] - ps[PP-1])
    numerical_thres = 1e-4
    if (is.null(uniformly_spaced)) {
        ps.gap = ps[2] - ps[1]
        gaps = ps[-1] - ps[-PP]
        uniformly_spaced = (sum(abs(gaps-ps.gap) > numerical_thres) == 0)
    }
    if (uniformly_spaced) {
        ps.gap = ps[2] - ps[1]
    } else {
        ps.gaps = rep(0, PP)
        for (k in 2:(PP+1)) {
            ps.gaps[k-1] = (extended.ps[k+1] - extended.ps[k-1])/2
        }
    }
    # Matrix where row l corresponds to fitted quantile function for level l,
    # column k corresponds to the fitted ps[k] quantiles:
    x = matrix(rep(0,PP*L), nrow = L) # different x than before.
    if (is.null(weights)) {
        seq.weights = sapply(quants, length)
    } else {
        seq.weights = sapply(weights,sum)  # the w_l^* in the TF algorithm.
    }
    mean.quants = rep(0,PP) # the (weighted) Frechet mean quantile-function. 
    for (l in 1:L) {
        sums = rep(0, PP)
        for (i in 1:length(quants[[l]])) {
            if (is.null(weights)) {
                sums = sums + quants[[l]][[i]]
            } else {
                sums = sums + (weights[[l]][i] * quants[[l]][[i]]) 
            }
        }
        x[l,] = sums / seq.weights[l]
        mean.quants = mean.quants + sums
    }
    mean.quants = mean.quants / sum(seq.weights)
    
    y = projectToLinearVectors(x, seq.weights, L, PP,ls)
    invalid.quantilefunctions = invalidQFs(y, L)
    projection.num = 0
    if (length(invalid.quantilefunctions) > 0) {
        r = x - y
        s = matrix(rep(0, L*PP), nrow = L, ncol = PP)
        dist.weights = rep(0, PP)
        for (k in 2:(PP+1)) {
            dist.weights[k-1] = (extended.ps[k+1] - extended.ps[k-1]) / 2
        }
    }
    last.y = matrix(rep(0, L*PP), nrow = L, ncol = PP) # TODO remove!
    while ((length(invalid.quantilefunctions) > 0) & (projection.num < max.iter)) {
        projection.num = projection.num + 1
        x = y + s # Initialize the projection for valid QFs.
        for (l in invalid.quantilefunctions) {  # project into valid quantile-function:
            if (uniformly_spaced) { 
                x[l,] = pava.sa(y[l,] + s[l,]) 
            } else {
                x[l,] = pava.sa(y[l,] + s[l,],
                             w = dist.weights)
            }
        }
        s = y + s - x
        y = projectToLinearVectors(x + r, seq.weights, L, PP, ls)
        invalid.quantilefunctions = invalidQFs(y, L)
        r = x + r - y
        # print(paste(projection.num,"th projection, max-change from last: ",max(y - last.y),sep=""))
        last.y = y # TODO remove!
    }
    if (projection.num >= max.iter) warning("max.iter reached without convergence")
    
    # Compute R^2:
    fitted.quantiles = y
    q.residuals = rep(0, PP)
    q.totalvariation = rep(0, PP)
    w_li = 1
    for (l in 1:L) {
        for (i in 1:length(quants[[l]])) {
            if (!is.null(weights)) { 
                w_li = weights[[l]][i]
            }
            q.residuals = q.residuals +
                w_li * (fitted.quantiles[l,] - quants[[l]][[i]])^2
            q.totalvariation = q.totalvariation +
                w_li * (mean.quants - quants[[l]][[i]])^2
        }
    }
        if (uniformly_spaced) {
        resid = sum(q.residuals) * ps.gap
        totalvar = sum(q.totalvariation) * ps.gap
        delta = sum(abs(fitted.quantiles[1,]-fitted.quantiles[L,])) * ps.gap / L
    } else {
        resid = sum(q.residuals * ps.gaps)
        totalvar = sum(q.totalvariation * ps.gaps)
        delta = sum(abs(fitted.quantiles[1,]-fitted.quantiles[L,]) * ps.gap) / L
    }
    if (totalvar < 1e-6) {
        R.sqr = 0
    } else {
        R.sqr = 1 - resid / totalvar
    }
    if (silent) {
       return( R.sqr )
    }
    return( list(R.sqr = R.sqr, Delta = delta, fq = fitted.quantiles) )
}


# Returns the projection into the space of trending vectors.
# mat is a matrix where the lth row denotes the quantile function,
# so the vector-sequence which becomes trending is given by mat's columns.
projectToLinearVectors <- function(mat, seq.weights = NULL, L, PP, ls=NULL) {
    fitted = mat
    if (is.null(ls)) {
       ls = 1:L
    }
    for (k in 1:PP) {
        fitted[,k] = lm(mat[,k] ~ ls, weights= seq.weights)$fitted.values
    }
    return( fitted )
}


# Permutation-testing to evaluate significance of fitted linear-TRENDS model. 
# trends_fit = fit to actual data.
# num_null_samples = the number of null samples desired to approximate the null distribution, 
# whether achievable through permutations alone, or together with boostrapping.
# Always goes through all permutations if number of possible datasets < allpermutations_thres.
getLinearTrendSignificance <- function(trends_fit, quants = NULL, ps = NULL, x = NULL,
                   ls = NULL, weights = NULL,
                   uniformly_spaced = NULL,
                   num_null_samples = 1000,
                   max.iter = 1e2,
                   bootstrap = NULL,
                   use_kernel = TRUE,
                   allpermutations_thres = 20,
                   seed = 12345 ) {
   set.seed(seed)
   L = length(quants)
   N = sum(sapply(quants, length))
   N_l = sapply(x, length)
   lis = numeric(0)
   for (l in 1:length(N_l)) { lis = c(lis, rep(l, N_l[l])) }
   rev_lis = L - lis + 1
   if (N < 100) {
      product = 1
      for (l in 1:L) {
          product = product * factorial(N_l[l])
      }
      num_possible = (factorial(N)-product-1)/2 # number of possible permutations with different TRENDS fits.
   } else {
      num_possible = 1e100
   } 
   if (is.null(x)) {
       if ((!is.null(bootstrap)) && bootstrap) {
           stop("cannot use bootstrap if x is NULL")
       } else {
           bootstrap = F
       }
   }
   if (is.null(ps)) {
        if (is.null(x)) {
            stop("ps and x cannot both be NULL")
        }
        ps = getDefaultProbs(x)
        quants = getQuantiles(x, ps)
        uniformly_spaced = TRUE
   }
   if ((num_null_samples >= num_possible/2) || 
       (num_possible <= allpermutations_thres)) { # we should evaluate all possible permutations:
      print('Going through all possible permutations')
      if (is.null(bootstrap)) bootstrap = TRUE
      permuted_statistics = numeric(0)
      # Use Dijkstra's method to go through all permutations:
      start_list = 1:N
      index_list = 1:N
      iter = 0
      while ((sum(index_list != start_list) > 0) || (iter == 0)) {
          iter = iter + 1;
          last_index = 0
          for (ii in 1:(N-1)) {
              if (index_list[ii] < index_list[ii+1]) last_index = ii
          }
          if (last_index == 0) {
              index_list = rev(index_list)
          } else {
              a = index_list[last_index]
              next_index = last_index + 1
              for (ii in next_index:N) {
                  if (index_list[ii] > a) next_index = ii  
              }
              b = index_list[next_index]
              index_list[last_index] = b
              index_list[next_index] = a
              index_list[(last_index+1):N] = index_list[N:(last_index+1)]
          }

          permuted_indices = index_list
          if (!(identical(lis[permuted_indices],lis) || 
                identical(lis[permuted_indices],rev_lis))) { # only consider permutations that lead to different labeling:
             # Given the permutation, compute permuted test-stat:
             if (bootstrap) { # repeat the following multiple times to generate multiple test-statistics for this permutation:
                 for (bb in 1:max(2,(ceiling(num_null_samples/num_possible)))) {
                     perm_x = list()
                     for (l in 1:L) { perm_x[[l]] = list() }
                     l = 1; index = 0; i = 0
                     while (index < N) {
                          index = index + 1
                          i = i + 1
                          if (i > N_l[l]) {
                              i = 1
                              l = l + 1
                          }
                          perm_index = permuted_indices[index]
                          total_count = 0
                          for (r in 1:L) {
                              total_count = total_count + N_l[r]
                              if (total_count >= perm_index) {
                                  perm_l = r
                                  perm_i = N_l[r] + perm_index - total_count
                                  break
                              }
                          }
                          perm_x[[l]][[i]] = sample(x[[perm_l]][[perm_i]],
                                              length(x[[perm_l]][[perm_i]]),replace=T)
                     }
                     perm_quants = getQuantiles(perm_x, ps)
                     permuted_statistics = c(permuted_statistics, 
                      fitLinearTrends(quants=perm_quants, ps=ps, x = NULL,  weights,
                                  ls = ls, uniformly_spaced, silent = TRUE))
                 }
             } else { # do not bootstrap:
                 perm_quants = list()
                 for (l in 1:L) { perm_quants[[l]] = list() }
                 l = 1; index = 0; i = 0
                 while (index < N) {
                     index = index + 1
                     i = i + 1
                     if (i > N_l[l]) {
                         i = 1
                         l = l + 1
                     }
                     perm_index = permuted_indices[index]
                     total_count = 0
                     for (r in 1:L) {
                         total_count = total_count + N_l[r]
                         if (total_count >= perm_index) {
                             perm_l = r
                             perm_i = N_l[r] + perm_index - total_count
                             break
                         }
                     }
                     perm_quants[[l]][[i]] = quants[[perm_l]][[perm_i]] 
                }
                permuted_statistics = c(permuted_statistics, 
                      fitLinearTrends(quants=perm_quants, ps=ps, x = NULL,  weights,
                                  ls = ls, uniformly_spaced, silent = TRUE))
             }
          }
      }
   } else { print('sampling permutations instead of going through all') 
      if (is.null(bootstrap)) bootstrap = FALSE
      permuted_statistics = rep(0, num_null_samples)
      for (zz in 1:num_null_samples) {
          permuted_indices = sample(N)
          # print(permuted_indices)
          while (identical(lis[permuted_indices],lis) || 
                identical(lis[permuted_indices],rev_lis)) { # only consider permutations that lead to different labeling:
             permuted_indices = sample(N)
          }
          if (bootstrap) {
             perm_x = list()
          } else {
            perm_quants = list()
          }
          for (l in 1:L) {
              if (bootstrap) {
                  perm_x[[l]] = list()
              } else {
                  perm_quants[[l]] = list()
              }
          }
          l = 1; index = 0; i = 0
          while (index < N) {
              index = index + 1
              i = i + 1
              if (i > N_l[l]) {
                  i = 1
                  l = l + 1
              }
              perm_index = permuted_indices[index]
              total_count = 0
              for (r in 1:L) {
                   total_count = total_count + N_l[r]
                   if (total_count >= perm_index) {
                        perm_l = r
                        perm_i = N_l[r] + perm_index - total_count
                        break
                    }
              }
              if (bootstrap) {
                 perm_x[[l]][[i]] = sample(x[[perm_l]][[perm_i]],
                                          length(x[[perm_l]][[perm_i]]),replace = T)
              } else {
                 perm_quants[[l]][[i]] = quants[[perm_l]][[perm_i]] 
              }
          }
          if (bootstrap) {
             perm_quants = getQuantiles(perm_x, ps)
          }
          permuted_statistics[zz] = fitLinearTrends(quants=perm_quants, ps=ps, x = NULL, ls=ls,
                                        weights= weights, uniformly_spaced, silent = TRUE)
       }
    }
    bw = NaN
    # print(permuted_statistics); print(trends_fit$R.sqr)
    if (use_kernel) bw = bw.CDF.pi(permuted_statistics, pilot="onestage")
    if (!is.nan(bw)) {   # print(paste('bandwidth:',bw))
       pval = 1-kCDF(permuted_statistics, xgrid=trends_fit$R.sqr,
                     bw=bw)$Fhat
    } else {
        num_greater = sum(permuted_statistics >= trends_fit$R.sqr)
        pval = (num_greater+1) / (length(permuted_statistics)+1) # actual p-value as per Wickham.
    }
    trends_fit$pval = pval
    return( trends_fit )
}


# Apply linear TRENDS to all genes in SCRS time-course.
# times must be list whose lth entry contains a list whose ith entry  
# is a vector of column indices indicating which cells in the expression matrix
# come from the ith batch at the lth time point. 
linearTrendSCRS <- function(exprmat, cell_times, parallel_cores = 4, padjust = "fdr",
                        ps = NULL, num_permutation=1000, bootstrap = NULL,
                        use_kernel = T, ls = NULL) {
    n_gene = nrow(exprmat)
    n_cell = ncol(exprmat)
    exprlist = split(exprmat, row(exprmat))
    if (sum(sapply(cell_times,function(z) sum(sapply(z,length)))) != n_cell) { stop("number of cell times does not equal number of cells in data") }
    if (padjust == "minP") {
        use_kernel = FALSE
    }
    oneGeneTrends <- function(g_expr) { # takes in vector of expression data and returns TRENDs fit.
        x_g = list()
        for (l in 1:length(cell_times)) {
            x_g[[l]] = list()
            for (i in 1:length(cell_times[[l]])) {
                x_g[[l]][[i]] = g_expr[cell_times[[l]][[i]]]
            }
        }
        if (!is.null(ps)) {
            quants=getQuantiles(x_g,ps)
            trends_fit = fitLinearTrends(quants=quants, ps = ps, x=x_g, ls=ls)
        } else {
            quants = NULL
            trends_fit = fitLinearTrends(x = x_g, ls=ls, uniformly_spaced=T)
        }
        if (num_permutation > 0) {
           trends_fit = getLinearTrendSignificance(trends_fit=trends_fit, quants=quants,
                   ps=ps, x=x_g, ls=ls,
                   weights = NULL, uniformly_spaced = NULL,
                   num_null_sample =num_permutation, 
                   bootstrap = bootstrap, use_kernel = use_kernel,
                   seed = 12345)
        } else { 
             trends_fit$pval = 0
        }
        return( c(trends_fit$R.sqr, trends_fit$Delta, trends_fit$pval) )
    }
    if (parallel_cores > 1) {
        cl <- makeCluster(parallel_cores)
        registerDoParallel(cl)
        clusterEvalQ(cl, library(sROC))
        clusterExport(cl=cl, varlist=c("cell_times", "ps", "num_permutation", "ls","bootstrap","use_kernel"), envir=environment())
        clusterExport(cl=cl, varlist=c(
                      "getQuantiles","fitLinearTrends","getLinearTrendSignificance","pava.sa","J","dykstra",
                      "invalidQFs","projectToLinearVectors"))
        results <- parLapply(cl, exprlist, oneGeneTrends)
        stopCluster(cl)
    } else {
        results <- lapply(exprlist, oneGeneTrends)
    }
    column_names = c("Rsqr","Delta","pval")
    results = matrix(unlist(results), ncol = length(column_names), byrow = T)
    colnames(results) = column_names 
    rownames(results) = rownames(exprmat)
    ordering = order(results[,3], -results[,2])
    if (is.null(padjust)) { # no multiple-testing correction:
        return( as.data.frame(results[ordering,]) )
    } else if (padjust != "minP") {
        adjusted_pvals = p.adjust(results[,'pval'], method=padjust)
    } else {
        # print('finished computing individual pvals. Now computing minP null pvals...')
        # Perform minP multiple-testing correction:
        actual_pvals = results[,'pval']
        pval_ordering = order(actual_pvals, decreasing = T)
        qs = rep(1, length(num_permutation))
        adjusted_pvals = rep(0, length(actual_pvals))
        for (jj in 1:length(pval_ordering)) { # step-down procedure:
            g = pval_ordering[jj]
            # if (jj %% 10 == 0) {
            #     print(paste('gene', jj,'out of',length(pval_ordering)))
            # }
            x_g = list()
            for (l in 1:length(cell_times)) {
                x_g[[l]] = list()
                for (i in 1:length(cell_times[[l]])) {
                    x_g[[l]][[i]] = exprmat[g,cell_times[[l]][[i]]]
                }
            }
            if (!is.null(ps)) {
                quants=getQuantiles(x_g,ps)
                trends_fit = fitLinearTrends(quants=quants, ps = ps, x=x_g, ls=ls)
            } else {
                quants = NULL
            }
            null_pvals = getLinearTrendsNullPvals(quants=quants, ps=ps, x=x_g, ls = ls,
                                      weights = NULL, uniformly_spaced = NULL, max_iter=max_iter,
                                      num_null_samples=num_permutation,
                                      seed = 12345, bootstrap = bootstrap)
            if (length(null_pvals) < length(qs)) {
                qs = null_pvals # can only happen once.
            }
            qs = pmin(qs, null_pvals)
            adjusted_pvals[g] = (sum(qs <= actual_pvals[g])+1)/(length(qs)+1)
        } # Finally enforce monotonicity:
        for (j in (length(adjusted_pvals)-1):1) {
            index = pval_ordering[j] # iterate in increasing order:
            adjusted_pvals[index] = max(adjusted_pvals[index],
                                        adjusted_pvals[pval_ordering[j+1]])
        }
    } # end of minP adjustment.
    results <- cbind(results,adjusted_pvals)
    colnames(results)[4] = 'p_adjusted'
    return( as.data.frame(results[ordering,]) )
}

## Used in minP method to efficiently compute pvalues under null distribution:
getLinearTrendsNullPvals <- function(quants = NULL, ps = NULL, x = NULL, ls = NULL,
                               weights = NULL, uniformly_spaced = NULL, max_iter = 1e3,
                               num_null_samples = 1000,
                               bootstrap = NULL,
                               allpermutations_thres = 20,
                               seed = 12345 ) {
    set.seed(seed)
    L = length(quants)
    N = sum(sapply(quants, length))
    N_l = sapply(x, length)
    lis = numeric(0)
    for (l in 1:length(N_l)) { lis = c(lis, rep(l, N_l[l])) }
    rev_lis = L - lis + 1
    if (N < 100) {
        product = 1
        for (l in 1:L) {
            product = product * factorial(N_l[l])
        }
        num_possible = (factorial(N)-product-1)/2 # number of possible permutations with different TRENDS fits.
    } else {
        num_possible = 1e100
    } 
    if (is.null(x)) {
        if ((!is.null(bootstrap)) && bootstrap) {
            stop("cannot use bootstrap if x is NULL")
        } else {
            bootstrap = F
        }
    }
    if (is.null(ps)) {
        if (is.null(x)) {
            stop("ps and x cannot both be NULL")
        }
        ps = getDefaultProbs(x)
        quants = getQuantiles(x, ps)
        uniformly_spaced = TRUE
    }
    if ((num_null_samples >= num_possible) || 
        (num_possible <= allpermutations_thres)) { # we should evaluate all possible permutations:
        # print('Going through all possible permutations')
        if (is.null(bootstrap)) bootstrap = TRUE
        permuted_statistics = numeric(0)
        # Use Dijkstra's method to go through all permutations:
        start_list = 1:N
        index_list = 1:N
        iter = 0
        while ((sum(index_list != start_list) > 0) || (iter == 0)) {
            iter = iter + 1;
            last_index = 0
            for (ii in 1:(N-1)) {
                if (index_list[ii] < index_list[ii+1]) last_index = ii
            }
            if (last_index == 0) {
                index_list = rev(index_list)
            } else {
                a = index_list[last_index]
                next_index = last_index + 1
                for (ii in next_index:N) {
                    if (index_list[ii] > a) next_index = ii  
                }
                b = index_list[next_index]
                index_list[last_index] = b
                index_list[next_index] = a
                index_list[(last_index+1):N] = index_list[N:(last_index+1)]
            }
            
            permuted_indices = index_list
            if (!(identical(lis[permuted_indices],lis) || 
                  identical(lis[permuted_indices],rev_lis))) { # only consider permutations that lead to different labeling:
                # Given the permutation, compute permuted test-stat:
                if (bootstrap) { # repeat the following multiple times to generate multiple test-statistics for this permutation:
                    for (bb in 1:max(2,(ceiling(num_null_samples/num_possible)))) {
                        perm_x = list()
                        for (l in 1:L) { perm_x[[l]] = list() }
                        l = 1; index = 0; i = 0
                        while (index < N) {
                            index = index + 1
                            i = i + 1
                            if (i > N_l[l]) {
                                i = 1
                                l = l + 1
                            }
                            perm_index = permuted_indices[index]
                            total_count = 0
                            for (r in 1:L) {
                                total_count = total_count + N_l[r]
                                if (total_count >= perm_index) {
                                    perm_l = r
                                    perm_i = N_l[r] + perm_index - total_count
                                    break
                                }
                            }
                            perm_x[[l]][[i]] = sample(x[[perm_l]][[perm_i]],
                                                      length(x[[perm_l]][[perm_i]]),replace=T)
                        }
                        perm_quants = getQuantiles(perm_x, ps)
                        permuted_statistics = c(permuted_statistics, 
                                                fitLinearTrends(quants=perm_quants, ps=ps, x = NULL, ls= ls,  weights=weights,
                                                            uniformly_spaced=uniformly_spaced, silent = TRUE))
                    }
                } else { # do not bootstrap:
                    perm_quants = list()
                    for (l in 1:L) { perm_quants[[l]] = list() }
                    l = 1; index = 0; i = 0
                    while (index < N) {
                        index = index + 1
                        i = i + 1
                        if (i > N_l[l]) {
                            i = 1
                            l = l + 1
                        }
                        perm_index = permuted_indices[index]
                        total_count = 0
                        for (r in 1:L) {
                            total_count = total_count + N_l[r]
                            if (total_count >= perm_index) {
                                perm_l = r
                                perm_i = N_l[r] + perm_index - total_count
                                break
                            }
                        }
                        perm_quants[[l]][[i]] = quants[[perm_l]][[perm_i]] 
                    }
                    permuted_statistics = c(permuted_statistics, 
                                            fitLinearTrends(quants=perm_quants, ps=ps, x = NULL, ls= ls, weights = weights,
                                                        uniformly_spaced = uniformly_spaced, silent = TRUE))
                }
            }
        }
    } else { # print('sampling permutations instead of going through all') 
        if (is.null(bootstrap)) bootstrap = FALSE
        permuted_statistics = rep(0, num_null_samples)
        for (zz in 1:num_null_samples) {
            permuted_indices = sample(N)
            # print(permuted_indices)
            while (identical(lis[permuted_indices],lis) || 
                   identical(lis[permuted_indices],rev_lis)) { # only consider permutations that lead to different labeling:
                permuted_indices = sample(N)
            }
            if (bootstrap) {
                perm_x = list()
            } else {
                perm_quants = list()
            }
            for (l in 1:L) {
                if (bootstrap) {
                    perm_x[[l]] = list()
                } else {
                    perm_quants[[l]] = list()
                }
            }
            l = 1; index = 0; i = 0
            while (index < N) {
                index = index + 1
                i = i + 1
                if (i > N_l[l]) {
                    i = 1
                    l = l + 1
                }
                perm_index = permuted_indices[index]
                total_count = 0
                for (r in 1:L) {
                    total_count = total_count + N_l[r]
                    if (total_count >= perm_index) {
                        perm_l = r
                        perm_i = N_l[r] + perm_index - total_count
                        break
                    }
                }
                if (bootstrap) {
                    perm_x[[l]][[i]] = sample(x[[perm_l]][[perm_i]],
                                              length(x[[perm_l]][[perm_i]]),replace = T)
                } else {
                    perm_quants[[l]][[i]] = quants[[perm_l]][[perm_i]] 
                }
            }
            if (bootstrap) {
                perm_quants = getQuantiles(perm_x, ps)
            }
            permuted_statistics[zz] = fitLinearTrends(quants=perm_quants, ps=ps, x = NULL, ls = ls, 
                                            weights=weights, uniformly_spaced = uniformly_spaced, silent = TRUE)
        }
    }
    permstat_ordering = order(permuted_statistics, decreasing = T)
    order_stats = table(permuted_statistics[permstat_ordering])
    null_pvals = 1:length(permuted_statistics)
    index = 0
    k_sum = 0
    for (i in 1:length(order_stats)) {
        num_unique = order_stats[i] # number of statistics tied at this value.
        k_sum = k_sum + num_unique
        null_pvals[permstat_ordering[(index+1):(index+num_unique)]] = k_sum
        index = index + num_unique
    }
    return( (null_pvals + 1)/(length(permuted_statistics)+1) )
}


